import json
from django.http import HttpResponse
from images.utils import image_to_json
from images.models import Image
from images.tasks import process_image
from PIL import Image as Pimage


def post_data(request):
    image = Pimage.open(request.FILES["image"].file)

    # celery
    task = process_image.delay(image_to_json(image))

    print(task.id)
    Image(task_id=task.id).save()

    return HttpResponse(json.dumps({
        "id":task.id,
    }))

def get_status(request, id):
    image: Image = Image.objects.get(task_id=id)
    return HttpResponse(json.dumps({
        "status": image.status # Image.TaskState(image.status).name
    }))

def image(request, id):
    print(id)
    # postgres
    image = Image.objects.get(task_id=id)
    return HttpResponse(json.dumps({
        "path":image.image.name
    }))
