
import json
from PIL import Image

import numpy as np


def image_to_json(image):
    return json.dumps(np.array(image).tolist())


def image_from_json(json_data):
    return Image.fromarray(np.array(json.loads(json_data), dtype='uint8'))
