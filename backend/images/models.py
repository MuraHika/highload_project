import json
import numpy as np
from django.db import models

class Image(models.Model):
    task_id = models.CharField(max_length=255)
    image = models.ImageField(null=True, upload_to='images')
    class TaskState(models.TextChoices):
            queued =   1, 'Queued'
            running =  2, 'Running'
            finished = 3, 'Finished'
            error =    4, 'Error'

    status = models.CharField(
        max_length=2,
        choices=TaskState.choices,
        default=TaskState.queued,
    )
