import os
from time import sleep
from uuid import uuid4
from images.utils import image_from_json
from images.models import Image
from core.celery import app
from PIL import Image as Pimage


@app.task(bind=True)
def process_image(self, image):
    task_id=self.request.id
    image = image_from_json(image)
    
    print(f"task {task_id}")

    image_model:Image = Image.objects.get(task_id=task_id)
    image_model.status = Image.TaskState.running
    image_model.save()

    try:
        fixed_image = image
        sleep(10)
        fixed_image_path = os.path.join("/media", f"{uuid4()}.png")
        print(f"image saved to {fixed_image_path}")
        fixed_image.save(fixed_image_path)

        image_model.image = fixed_image_path
        image_model.save()
    except:
        image_model.status = Image.TaskState.error
        image_model.save()

    image_model.status = Image.TaskState.finished
    image_model.save()
