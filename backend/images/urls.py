from django.urls import path
from images.views import get_status, image, post_data

urlpatterns = [
    path('post_data/', post_data),
    path('status/<id>/', get_status),
    path('image/<id>/', image),
]
