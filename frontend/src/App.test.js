import { render, screen } from '@testing-library/react';
import Enzyme, { shallow, mount } from 'enzyme';
import App from './App';
import Task from './Task';

const task = { id: 'id_task', status: '3', path: './media/path', message: 'Ваш диагноз готов!'}

it("renders without crashing", () => {
  shallow(<App />);
});

it("render success message", () => {
  const wrapper = shallow(<Task task={task}/>);

  const success = <p className="success">Ваш диагноз готов!</p>;
  expect(wrapper.contains(success)).toEqual(true);
});

it("render link to download picture", () => {
  const wrapper = shallow(<Task task={task}/>);

  const link = <p><a href={task.path}>Открыть файл в браузере</a></p>;
  expect(wrapper.contains(link)).toEqual(true);
});