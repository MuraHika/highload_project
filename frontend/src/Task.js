import React from "react";

function Task(props) {
  const { task } = props;

  return(
  <div className="card-task" key={task.id}>
    <p>Задача: {task.id}</p>

    {task.status !== '3' && <div class="spinner-border text-primary" role="status">
      <span class="sr-only">Loading...</span>
    </div>}
    {task.status === '4' && <p>Произошла неожиданная ошибка</p>}
    <p className={task.status === '2' ? "error" : "success"}>{task.message}</p>
    {task.path !== '' && <p><a href={task.path}>Открыть файл в браузере</a></p>}
    {task.path !== '' && <p><a href={task.path} download>Скачать файл</a></p>}
</div>
);
}

export default Task;