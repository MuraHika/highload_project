import React, { useState } from "react";
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Task from "./Task";
import './App.css';

function App() {
  const [age, setAge] = useState('');
  const [height, setHeight] = useState('');
  const [weight, setWeight] = useState('');
  const [image, setImage] = useState(null);
  const [listImages, setListImages] = useState([]);

  const postData = async () => {
    const formData = new FormData();
    formData.append("image", image);
    formData.append("age", age);
    formData.append("height", height);
    formData.append("weight", weight);
    try {
      const header = {
        method: "post",
        url: "/api/post_data/",
        data: formData,
        headers: { "Content-Type": "multipart/form-data" },
      };
      console.log(header);
      await axios(header).then((response) => {
        console.log(response);
        setListImages(prevState => [...prevState, { id: response.data.id, status: '1', path: '', message: ''}]);
        loadImage(response.data.id);
      });
    } catch(error) {
      console.log(error)
    }
  };

  const loadImage = async (id) => {
    let status = '1';
    while( status !== '3'){
      await new Promise(resolve => setTimeout(resolve, 1500));
      try {
        const response = await axios.get(`/api/status/${id}/`);
        console.log(response.data);
        if (response.data.status === '2') {
          status = '2';
          setListImages(prevState => 
            prevState.map(item => 
              item.id === id 
                ? { ...item, message: 'Данные обрабатывается...', status: '2' }
                : item
            ));
        } else if (response.data.status === '3') {
          status = '3';
          await axios.get(`/api/image/${id}/`).then(imageResponse => {
            console.log('path',imageResponse.data.path);
            setListImages(prevState => 
              prevState.map(item => 
                item.id === id 
                  ? { ...item,  message: 'Ваш диагноз готов!', path: imageResponse.data.path, status: '3' }
                  : item
            ));
          });
        }
      } catch(error) {
        console.log(error)
      }
      
    }
  }

  const handlerChangeAge = (event) => {
    setAge(event.target.value);
  }

  const handlerChangeHeight = (event) => {
    setHeight(event.target.value);
  }

  const handlerChangeWeight = (event) => {
    setWeight(event.target.value);
  }

  return (
    <div className="App">
      <div className="header">
        <p>Введите ваши данные:</p>
        <p>Возраст: </p>
        <input className="" type="text" value={age} onChange={(e) => handlerChangeAge(e)} />
        <p>Рост: </p>
        <input className="" type="text" value={height} onChange={(e) => handlerChangeHeight(e)} />
        <p>Вес: </p>
        <input className="" type="text" value={weight} onChange={(e) => handlerChangeWeight(e)} />
        <div className="header-load">
          <label htmlFor="formFileDisabled" className="form-label">Выберите файл:</label>
          <input className="form-control" type="file" id="formFileDisabled" onChange={(e) => setImage(e.target.files[0])} />
        </div>
        <Button variant="primary" onClick={() => postData()}>Отправить</Button>
      </div>

      {listImages.map((el) => {
          return (
            <Task task={el} />
          )
      })}
    </div>
  );
}

export default App;
